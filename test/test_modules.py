import unittest
from ml4proflow import modules
from ml4proflow_mods.bayesianoptimization.modules import BOModule


class TestModulesModule(unittest.TestCase):
    def setUp(self):
        self.dfm = modules.DataFlowManager()

    def test_create_ommodule(self):
        dut = BOModule(self.dfm, {})
        self.assertIsInstance(dut, BOModule)


if __name__ == '__main__':  # pragma: no cover
    unittest.main()
