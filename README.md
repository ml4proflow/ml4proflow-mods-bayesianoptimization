# ml4proflow-mods-bayesianoptimization

A module for ml4proflow to perform a bayesian optimization

[![Tests Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/tests-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/file/reports/junit/report.html?job=gen-cov)
[![Coverage Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/coverage-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/file/reports/coverage/index.html?job=gen-cov)
[![Flake8 Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/flake8-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/file/reports/flake8/index.html?job=gen-cov)
[![mypy errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/mypy.svg?job=gen-cov)]()
[![mypy strict errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-openmodelica/-/jobs/artifacts/master/raw/mypy_strict.svg?job=gen-cov)]()
------------
# Usage 
To configure the module correctly, add the following module configuration to the configuration file: 
```json 
{
    "Maximum Iterations" : 100,     # Loop iteration
    "Level of verbosity" : 2,
    "Random State" : 1,
    "Utility Function" : {
        "Kind" : "",
        "Kappa" : 0.0,
        "xi" : 0.0
    },
    "Data Interpretation": {
        "Target" : "target_name",   # Name of Target
        "Parameter": {              # Name of Parameter and their lower & upper bounds
            "x" : [-100.0, 100,0],
            "y" : [-5, 5],
            "z" : [0, 10]
        }
    }
}
```

For more information, see [here](https://github.com/fmfn/BayesianOptimization)

Necessary keys:
- Name of target
- Parameter bounds
- Kind of utility function

# Prerequisites
- [ml4proflow](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow)

# Installation
Activate your virtual environment, clone this repository and install the package with pip:
```console 
$ pip install .
```

# Contribution
```console 
$ pip install -e .
```
