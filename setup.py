from setuptools import setup, find_namespace_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

name = "ml4proflow-mods-bayesianoptimization"
version = "0.0.1"

cmdclass = {}

try:
    from sphinx.setup_command import BuildDoc
    cmdclass['build_sphinx'] = BuildDoc
except ImportError:
    print('WARNING: Sphinx not available, not building docs')

setup(
    name=name,
    version=version,
    author="Dennis Quirin",
    author_email="dquirin@techfak.uni-bielefeld.de",
    description="ml4proflow-Module to perform bayesian optimization",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="todo",
    project_urls={
        "Main framework": "todo",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=find_namespace_packages(where="src"),
    namespace_packages=['ml4proflow_mods'],
    package_dir={"": "src"},
    # package_data={"ml4proflow": ["py.typed"], "OMPython": ["py.typed"]},
    entry_points={
        # 'console_scripts': [
        # 'ml4proflow-cli=ml4proflow.ml4proflow_cli:main',
        # ],
    },
    cmdclass=cmdclass,
    python_requires=">=3.6",  # todo
    install_requires=[
        "ml4proflow",
        "bayesian-optimization"],
    extras_require={
        "tests": ["pytest"],
        "docs": ["sphinx", "sphinx-rtd-theme", "recommonmark"],
    },
    command_options={
        'build_sphinx': {
            'project': ('setup.py', name),
            'version': ('setup.py', version),
            'release': ('setup.py', version),
            'source_dir': ('setup.py', 'docs/source/'),
            'build_dir': ('setup.py', 'docs/build/')
        }
    },
)
