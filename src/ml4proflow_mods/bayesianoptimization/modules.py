from __future__ import annotations
from typing import Any
import pandas

from ml4proflow.modules import (Module,
                                DataFlowManager, SourceModule)
from bayes_opt import (BayesianOptimization, UtilityFunction)


class BOModule(Module):
    def __init__(self, dfm: DataFlowManager, config: dict[str, Any]):
        Module.__init__(self, dfm, config)
        self.maxiter = config.setdefault('Maximum Iterations', 10)
        self.verbosity_state = config.setdefault('Level of verbosity', 2)
        self.randomState = config.setdefault('Random State', 1)

        self.kind = config.setdefault('Utility Function',
                                      {}).setdefault('Kind', 'ucb')
        self.kappa = config.setdefault('Utility Function',
                                       {}).setdefault('Kappa', 2.5)
        self.xi = config.setdefault('Utility Function',
                                    {}).setdefault('xi', 0.0)

        self.data_interpretation = config.setdefault('Data Interpretation',
                                                     {'Target': 'target',
                                                      'Parameter':
                                                      {'x': (0, 0)}})
        self.bounds = self.data_interpretation['Parameter']
        self.param = self.data_interpretation['Parameter']

        self.count = 0
        self.mode = 'Init'

        self.optimizer = BayesianOptimization(
            f=None,
            pbounds=self.bounds,
            verbose=self.verbosity_state,
            random_state=self.randomState
        )

        self.utility = UtilityFunction(kind=self.kind,
                                       kappa=self.kappa, xi=self.xi)

    def on_new_data(self, name: str, sender: SourceModule,
                    data: pandas.DataFrame) -> None:
        """Helper function:
        On Event "New Data", this function converts the DataFrame
        into valid Dicts to perform the baysian optimization process.
        The suggestion for the next Points will be pushed
        or the resulting optimum.


        Not supported features (ToDo?):
        - Just scalar values are allowed (target & nextPoints)
        - Registered points are not stored
        - No inner optimization loop (constructed trough DFG configuration)

        :param name: Name of the channel
        :param sender: Module, providing the data
        :param data: Provided data frame
        :return: None
        """

        target = data.at[0, self.data_interpretation['target']]
        for colum in data.columns.values.tolist():
            if colum in list(self.bounds.keys()):
                self.param[colum] = data.at[0, colum]

        if self.count > 0:
            self.mode = 'OptimizationLoop'
        elif self.count >= self.maxiter:
            self.mode = 'FoundOptimum'

        if self.mode != 'Init':
            self.optimizer.register(params=self.param, target=target)

        nextPoints = self.optimizer.suggest(self.utility)

        if self.mode == 'FoundOptimum':
            optim = self.optimizer.max
            param_optim = optim['params']
            optim.pop('params')
            optim.update(param_optim)
            df = pandas.DataFrame(optim, index=[0])
        else:
            df = pandas.DataFrame(nextPoints, index=[0])

        self.count += 1
        self._push_data(self.channel_push[0], df)
